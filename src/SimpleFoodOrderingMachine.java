import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane receivedInfo;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JLabel orderdItems;
    private JButton yakisobaButton;
    private JButton checkout;
    private JLabel total;
    private JLabel totalPrice;

    int num = 0; /* 注文数 */

    void order(String food){
        int confirmation =JOptionPane.showConfirmDialog(null,
                "Would you like to "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering "+food+"! It will be served as soon as possible.");
            receivedInfo.setText(receivedInfo.getText()+food+"\n");
            num++;
            totalPrice.setText(num*100+" yen");
        }
    }

    public SimpleFoodOrderingMachine() {

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation =JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + num*100 + " yen.");
                    receivedInfo.setText("");
                    num=0;
                    totalPrice.setText("0 yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
